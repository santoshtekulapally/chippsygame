package com.example.chippsygame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.ArrayList;

public class Enemy {


    public int xPosition;
    public int yPosition;
    public int width;
    private int speed;
    private Bitmap image;
    private Rect hitbox;

    //Rect hitbox;


    // GETTER AND SETTER METHODS
    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Enemy(Context context, int x, int y, int width, int speed) {

        this.xPosition = x;
        this.yPosition = y;
        this.width = width;
        this.speed = speed;

        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.bee);


        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.width,
                this.yPosition + this.width
        );

    }

    public int getSpeed() {
        return this.speed;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public int getWidth() {
        return width;
    }

    public Rect getHitbox() {
        return hitbox;
    }
    public void updateHitbox() {
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.right = this.xPosition + this.image.getWidth();
        this.hitbox.bottom = this.yPosition + this.image.getHeight();


    }
    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}




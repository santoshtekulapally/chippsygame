package com.example.chippsygame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine  extends SurfaceView implements Runnable {

    // -----------------------------------
    // ## ANDROID DEBUG VARIABLES
    // -----------------------------------

    // Android debug variables
    final static String TAG = "CHIPPY";

    // -----------------------------------
    // ## SCREEN & DRAWING SETUP VARIABLES
    // -----------------------------------

    // screen size
    int screenHeight;
    int screenWidth;
    int SQUARE_WIDTH = 200;
    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;


    // -----------------------------------
    // ## GAME SPECIFIC VARIABLES
    // -----------------------------------
    final int BOTTOM_OF_SCREEN = 700;
    final int TOP_OF_SCREEN = 100;
    final int RIGHT_OF_SCREEN = 1600;
    final int LEFT_OF_SCREEN = 100;

    Direction bulletDirection = Direction.DOWN;
    // ----------------------------
    // ## SPRITES
    // ----------------------------

    Player player;
    Enemy enemy;

    Bitmap background;
    int bgXposition = 0;
    int backgroundRightside = 0;

    // ----------------------------
    // ## GAME STATS - number of lives, score, etc
    // ----------------------------

    public enum Direction {
        UP,
        DOWN,
        RIGHT,
        LEFT
    }
    public GameEngine(Context context, int w, int h) {
        super(context);




        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        this.printScreenInfo();

        // @TODO: Add your sprites to this section
        // This is optional. Use it to:
        //  - setup or configure your sprites
        //  - set the initial position of your sprites
        this.player = new Player(getContext(), 100, 500);
        this.enemy = new Enemy(context, 700, 100, SQUARE_WIDTH, 2);


        this.background = BitmapFactory.decodeResource(context.getResources(), R.drawable.space);
        this.background = Bitmap.createScaledBitmap(this.background, this.screenWidth, this.screenHeight, false);
        this.bgXposition = 0;


        // @TODO: Any other game setup stuff goes here


    }

    // ------------------------------
    // HELPER FUNCTIONS
    // ------------------------------

    // This funciton prints the screen height & width to the screen.
    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }

    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }


    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    int numLoops = 0;

    // 1. Tell Android the (x,y) positions of your sprites


    public void updatePositions() {
        // @TODO: Update the position of the sprites

        this.bgXposition = this.bgXposition - 50;

        backgroundRightside = this.bgXposition + this.background.getWidth();

        if (backgroundRightside < 0) {
            this.bgXposition = 0;
        }
        numLoops = numLoops + 1;


        if (numLoops % 3 == 0) {
            this.player.spawnBullet();
        }


        if (bulletDirection == Direction.DOWN) {

            enemy.yPosition = enemy.yPosition + 10;

            // enemy.xPosition = enemy.xPosition - 10;


            // enemy.xPosition = enemy.xPosition + 10;

            if (enemy.yPosition > BOTTOM_OF_SCREEN) {

                bulletDirection = Direction.RIGHT;

                enemy.xPosition = enemy.xPosition + 10;

                //enemy.yPosition = enemy.yPosition - 10;
            }

        }
        if (bulletDirection == Direction.UP) {

            enemy.yPosition = enemy.yPosition - 10;
            // enemy.xPosition = enemy.xPosition - 10;


            if (enemy.yPosition < TOP_OF_SCREEN) {


                bulletDirection = Direction.DOWN;

                // enemy.yPosition = enemy.yPosition - 10;
                //new one
            }


        }

        if (bulletDirection == Direction.RIGHT) {

            enemy.xPosition = enemy.xPosition + 10;


            if (enemy.xPosition > RIGHT_OF_SCREEN) {


                bulletDirection = Direction.UP;

                System.out.print("milgaya"+enemy.xPosition + enemy.yPosition);

                enemy.yPosition = enemy.yPosition - 10;

            }

        }






        int BULLET_SPEED = 50;
        for (int i = 0; i < this.player.getBullets().size(); i++) {
            Rect bullet = this.player.getBullets().get(i);
            bullet.left = bullet.left + BULLET_SPEED;
            bullet.right = bullet.right + BULLET_SPEED;
        }

        // COLLISION DETECTION ON THE BULLET AND WALL
        for (int i = 0; i < this.player.getBullets().size(); i++) {
            Rect bullet = this.player.getBullets().get(i);

            // For each bullet, check if the bullet touched the wall
            if (bullet.left > this.screenWidth) {
                this.player.getBullets().remove(bullet);
            }

        }

        // COLLISION DETECTION BETWEEN BULLET AND PLAYER
        for (int i = 0; i < this.player.getBullets().size(); i++) {
            Rect bullet = this.player.getBullets().get(i);

            if (this.enemy.getHitbox().intersect(bullet)) {
                this.enemy.setxPosition(1600);
                this.enemy.setyPosition(500);
                this.enemy.updateHitbox();
            }
        }


        // @TODO: Update position of player based on mouse tap
        if (this.fingerAction == "mousedown") {

            player.setxPosition(player.x);
            player.setyPosition(player.y);
            player.updateHitbox();


        }

        // @TODO: Collision detection code

    }

    // 2. Tell Android to DRAW the sprites at their positions
    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------
            // Put all your drawing code in this section

            // configure the drawing tools
//            this.canvas.drawColor(Color.argb(255,0,0,255));
            paintbrush.setColor(Color.WHITE);

            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLACK);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);


            //@TODO: Draw the sprites (rectangle, circle, etc)
            canvas.drawBitmap(this.background, this.bgXposition, 0, paintbrush);

            canvas.drawBitmap(this.background, this.backgroundRightside, 0, paintbrush);

            // draw player graphic on screen

            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            // draw the player's hitbox
            canvas.drawRect(player.getHitbox(), paintbrush);


            canvas.drawBitmap(enemy.getImage(), enemy.getxPosition(), enemy.getyPosition(), paintbrush);
            canvas.drawRect(enemy.getHitbox(), paintbrush);

            for (int i = 0; i < this.player.getBullets().size(); i++) {
                Rect bullet = this.player.getBullets().get(i);
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setColor(Color.RED);
                canvas.drawRect(bullet, paintbrush);

            }
            // draw enemy
            paintbrush.setColor(Color.MAGENTA);
            canvas.drawRect(
                    this.enemy.getxPosition(),
                    this.enemy.getyPosition(),
                    this.enemy.getxPosition() + this.enemy.getWidth(),
                    this.enemy.getyPosition() + this.enemy.getWidth(),
                    paintbrush
            );

            //@TODO: Draw game statistics (lives, score, etc)
            paintbrush.setTextSize(60);
            canvas.drawText("Score: 25", 20, 100, paintbrush);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    // Sets the frame rate of the game
    public void setFPS() {
        try {
            gameThread.sleep(30);
        } catch (Exception e) {

        }
    }

    String fingerAction = "";

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_MOVE:

                player.x = (int) event.getX();
                player.y = (int) event.getY();

                Log.d("PUSH", "PERSON CLICKED AT: (" + event.getX() + "," + event.getY() + ")");
                fingerAction = "mousedown";
                break;

        }
        return true;
    }
}

